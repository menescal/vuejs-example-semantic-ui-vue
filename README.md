Simple Example Using Semantic UI Vue
---
**Author: Menescal, Daniel M.**

##Quick Start, Follow the steps.

### Install Vue
```
npm install -g @vue/cli		
```	

### Install Json Server 
```
npm install -g json-server	
```	

### Install the dependencies
```
npm install
```

### Run de project 
```
npm start
```

### Run Json Server	
```
json-server db\db.json
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
	
Refs Semantic Ui Vue: [https://semantic-ui-vue.github.io](https://semantic-ui-vue.github.io)

