/* eslint-disable */
import axios from 'axios'

const client = axios.create({
    baseURL: 'http://localhost:3000/',
    json: true
})

export default {
    async execute(method, resource, data) {
        return client({
            method,
            url: resource,
            data
        }).then(res => {
            return res.data
        }).catch(error => {
            return error.response;           
        })        
    },
    
    getOrganizations() {
        return this.execute('get', `organizations`)
    }
}